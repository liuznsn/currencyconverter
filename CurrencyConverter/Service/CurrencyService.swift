//
//  CurrencyService.swift
//  CurrencyConverter
//
//  Created by Leo on 10/7/18.
//  Copyright © 2018 Leo. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa


public protocol CurrencyServiceType{
    func updateRates(type: CurrencyType) -> Observable<(rates:[String : Double]?,base:CurrencyType?)>
    func currencies(type:CurrencyType) -> Observable<[Currency]?>
}

public class CurrencyService:CurrencyServiceType{
    
    static let sharedAPI = CurrencyService()
    
    public func currencies(type: CurrencyType) -> Observable<[Currency]?> {
        let emptyResult:[Currency]? = []
        
        guard let url = self.url(for: type.text) else { return .just(emptyResult) }
        return URLSession.shared.rx.json(url: url)
            .map { json -> ([Currency]?) in
                
            guard let dict = json as? [String: Any] else { return emptyResult }
            guard let rates = dict["rates"] as? [String: Any] else { return emptyResult }
                
            var currencies = [Currency]()
            rates.forEach { rate in
                var currency = Currency()
                currency.base = CurrencyType(rawValue: dict["base"] as! String)
                currency.date = dict["date"] as? String
                currency.type = CurrencyType(rawValue: rate.key)
                currency.rate = rate.value as? Double
                currencies.append(currency)
            }
                
                var currency = Currency()
                currency.base = CurrencyType(rawValue: dict["base"] as! String)
                currency.date = dict["date"] as? String
                currency.type = CurrencyType(rawValue: "EUR")
                currency.rate = 1
                currencies.insert(currency, at: 0)
                
                return currencies
                
            }
            .do(onError: { error in
                if case let .some(.httpRequestFailed(response, _)) = error as? RxCocoaURLError, response.statusCode == 403 {
                    print("⚠️.")
                }
            })
            .catchErrorJustReturn(emptyResult)
    }
    

    public func updateRates(type: CurrencyType) -> Observable<(rates:[String : Double]?,base:CurrencyType?)> {
        
        let emptyResult:[String : Double]? = [:]
    
        guard let url = self.url(for: type.text) else { return .just((emptyResult,nil)) }
        return URLSession.shared.rx.json(url: url)
            .observeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .map { json -> ([String : Double]?,CurrencyType?) in
                var currencyType:CurrencyType?
                var reates:[String : Double]?
                guard let dict = json as? [String: Any] else { return (reates,currencyType) }
                currencyType = CurrencyType(rawValue: dict["base"] as! String)
                reates = dict["rates"] as? [String : Double]
                return (reates,currencyType!)
                
            }
            .do(onError: { error in
                if case let .some(.httpRequestFailed(response, _)) = error as? RxCocoaURLError, response.statusCode == 403 {
                    print("⚠️.")
                }
            })
            .catchErrorJustReturn((emptyResult,nil))
        
    }
    
    private func url(for base: String?) -> URL? {
        guard let query = base, !base!.isEmpty else { return nil }
        return URL(string: "https://revolut.duckdns.org/latest?base=\(query)")
    }
    
}
