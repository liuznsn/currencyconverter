//
//  Enum.swift
//  CurrencyConverter
//
//  Created by Leo on 10/7/18.
//  Copyright © 2018 Leo. All rights reserved.
//

public enum CurrencyType: String {
    
    public static var count: Int {
        return currency.count
    }
    
    public var text: String? {
        return currency.filter{ $0 == rawValue }.first
    }
    
    public var fullName: String? {
        return currencyFullName.filter{ $0.key == rawValue }.first?.value
    }
    
    case EUR,AUD,BGN,BRL,CAD,CHF,CNY,CZK,DKK,GBP,HKD,HRK,HUF,IDR,ILS,INR,ISK,JPY,KRW,MXN,MYR,NOK,NZD,PHP,PLN,RON,RUB,SEK,SGD,THB,TRY,USD,ZAR
}

private let currency = ["EUR","AUD","BGN","BRL","CAD","CHF","CNY","CZK","DKK","GBP","HKD","HRK","HUF","IDR","ILS","INR","ISK","JPY","KRW","MXN","MYR","NOK","NZD","PHP","PLN","RON","RUB","SEK","SGD","THB","TRY","USD","ZAR"]

private let currencyFullName = [
    "AUD":"Australian dollar",
    "BRL":"Brazilian real",
    "GBP":"British pound",
    "BGN":"Bulgarian lev",
    "CAD":"Canadian dollar",
    "CNY":"Chinese yuan",
    "HRK":"Croatian kuna",
    "CZK":"Czech koruna",
    "DKK":"Danish krone",
    "EUR":"Euro",
    "HKD":"Hong Kong dollar",
    "HUF":"Hungarian forint",
    "INR":"Indian rupee",
    "IDR":"Indonesian rupiah",
    "ILS":"Israeli new sheqel",
    "JPY":"Japanese yen",
    "MYR":"Malaysian ringgit",
    "MXN":"Mexican peso",
    "NZD":"New Zealand dollar",
    "NOK":"Norwegian krone",
    "PHP":"Philippine peso",
    "PLN":"Polish złoty",
    "RON":"Romanian leu",
    "RUB":"Russian ruble",
    "SGD":"Singapore dollar",
    "ZAR":"South African rand",
    "KRW":"South Korean won",
    "SEK":"Swedish krona",
    "CHF":"Swiss franc",
    "THB":"Thai baht",
    "TRY":"Turkish lira",
    "USD":"United States dollar",
    "ISK":"Iceland Krona"
]
