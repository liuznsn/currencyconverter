//
//  Currency.swift
//  CurrencyConverter
//
//  Created by Leo on 10/7/18.
//  Copyright © 2018 Leo. All rights reserved.
//

import Foundation

public struct Currency{
    
    var base:CurrencyType?
    var type:CurrencyType?
    var date:String?
    var rate:Double?
    
}
