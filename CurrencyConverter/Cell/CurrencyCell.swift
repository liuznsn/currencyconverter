//
//  CurrencyCell.swift
//  CurrencyConverter
//
//  Created by Leo on 10/7/18.
//  Copyright © 2018 Leo. All rights reserved.
//

import UIKit
import RxSwift

class CurrencyCell: UITableViewCell {

    private(set) var disposeBag = DisposeBag()

    var currency:Currency?
    var rate:Double?
    var currencyType:CurrencyType?
    
    lazy var countryFlag:UIImageView = {
       let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = (40 / 2)
        imageView.layer.masksToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    lazy var shortNameLabel:UILabel = {
        let label = UILabel()
        label.backgroundColor = .clear
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    lazy var fullNameLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        label.backgroundColor = .clear
        label.adjustsFontSizeToFitWidth = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    lazy var amountField:CurrencyTextField = {
        let textField = CurrencyTextField()
        textField.textAlignment = .right
        textField.font = UIFont.systemFont(ofSize: 22)
        textField.keyboardType = UIKeyboardType.numberPad
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()
    
    override func prepareForReuse() {
        super.prepareForReuse()
        disposeBag = DisposeBag()
    }
    
    convenience init(currency:Currency) {
      self.init(style: .default, reuseIdentifier: "CurrencyCell")
      self.currency = currency
      self.currencyType = self.currency?.type
      self.rate = self.currency!.rate!
      self.shortNameLabel.text = self.currency?.type!.text
      self.fullNameLabel.text = self.currency?.type!.fullName
      self.countryFlag.image = UIImage(named: "flag_\(self.currencyType!.text!.lowercased())")
        
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.initialize()
        self.setConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func initialize() {
        self.addSubview(self.countryFlag)
        self.addSubview(self.shortNameLabel)
        self.addSubview(self.fullNameLabel)
        self.addSubview(self.amountField)
    }
    
    func setConstraints() {
        self.countryFlag.widthAnchor(equalTo: 40)
        self.countryFlag.heightAnchor(equalTo: 40)
        self.countryFlag.leadingAnchor(equalTo: self.leadingAnchor, constant: 10)
        self.countryFlag.topAnchor(equalTo: self.topAnchor, constant: 15)
        self.countryFlag.bottomAnchor(equalTo: self.bottomAnchor, constant: -15)
        
        self.shortNameLabel.widthAnchor(equalTo: 50)
        self.shortNameLabel.heightAnchor(equalTo: 20)
        self.shortNameLabel.leadingAnchor(equalTo: self.countryFlag.trailingAnchor, constant: 10)
        self.shortNameLabel.topAnchor(equalTo: self.topAnchor, constant: 10)
        
        self.fullNameLabel.widthAnchor(equalTo: 100)
        self.fullNameLabel.heightAnchor(equalTo: 20)
        self.fullNameLabel.leadingAnchor(equalTo: self.countryFlag.trailingAnchor, constant: 10)
        self.fullNameLabel.topAnchor(equalTo: self.shortNameLabel.bottomAnchor, constant: 10)
    
        self.amountField.leadingAnchor(equalTo: self.fullNameLabel.trailingAnchor, constant: 10)
        self.amountField.topAnchor(equalTo: self.topAnchor, constant: 10)
        self.amountField.bottomAnchor(equalTo: self.bottomAnchor, constant: -10)
        self.amountField.trailingAnchor(equalTo: self.trailingAnchor, constant: -10)
    }

}
