//
//  ConverterViewController.swift
//  CurrencyConverter
//
//  Created by Leo on 2018/10/09.
//  Copyright © 2018 Leo. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources

class ConverterViewController: UIViewController, UITableViewDelegate {

    var disposeBag = DisposeBag()
    fileprivate var dataSource:RxTableViewSectionedReloadDataSource<SectionModel<String,Currency>>!
    
     //global variable
    let items = BehaviorRelay(value: [Currency]())
    let mainCurrencyValue = BehaviorRelay(value: Double(100))
    let newestRates = BehaviorRelay(value: [String:Double]())
    var currentType:CurrencyType = .EUR //default currency
    
    //update event
    let updateBaseCurrency = PublishSubject<CurrencyType>()
    let updateRatesToVisibleCells = PublishSubject<[String:Double]>()
    
    lazy var tableView:UITableView = {
        let tv = UITableView(frame: UIScreen.main.bounds)
        tv.rx.setDelegate(self)
            .disposed(by: disposeBag)
        tv.register(CurrencyCell.self, forCellReuseIdentifier: "CurrencyCell")
        tv.allowsSelection = false
        tv.rowHeight = UITableView.automaticDimension
        tv.keyboardDismissMode = .onDrag
        return tv
    }()
    
   override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Converter"
        self.view = self.tableView
        self.edgesForExtendedLayout = .init(rawValue: 0)
        self.dataSource = self.dataSourceFactory()
        bindRx()
   }
   
   func bindRx() {
    //fetch currencies from the API, and default as .EUR
    CurrencyService.sharedAPI
        .currencies(type: currentType)
        .do(onNext: { items  in
            self.items.accept(items!)
            var newestRates:[String:Double] = [:]
            items?.forEach({ currency in
                let type = currency.type
                newestRates.updateValue(currency.rate!, forKey:(type?.text)!)
            })
            self.newestRates.accept(newestRates)
        })
        .subscribe()
        .disposed(by: disposeBag)
    
    //set datasource to the tableview
    items
        .map{ [SectionModel<String, Currency>(model: "currencySection", items: $0)] }
        .bind(to: self.tableView.rx.items(dataSource: self.dataSource))
        .disposed(by: disposeBag)
    
    //per 1 second to update currency rates from the API
    Observable<Int>
        .interval(1.0, scheduler: MainScheduler.instance)
        .subscribe(onNext: { number in
            
            CurrencyService.sharedAPI
                .updateRates(type: self.currentType)
                .subscribe(onNext: { (rates,baseType) in
                    if baseType != self.currentType || self.newestRates.value == rates{ // get rates isn't equal current currency
                        return
                    }
                    DispatchQueue.main.sync {
                        self.updateNewestRatesToVisibleCells(rates: rates!)
                        self.newestRates.accept(rates!)
                    }
                }).disposed(by: self.disposeBag)
            
        }).disposed(by: self.disposeBag)
    
    //update newest rates
    updateRatesToVisibleCells
        .catchErrorJustReturn([:])
        .subscribe(onNext: { rates in
            DispatchQueue.main.sync {
                self.updateNewestRatesToVisibleCells(rates: rates)
            }
        }).disposed(by: disposeBag)
    
   }
    
    func updateNewestRatesToVisibleCells(rates:[String:Double]){
        for cell in self.tableView.visibleCells {
            let cell = cell as! CurrencyCell
            if rates[(cell.currencyType?.text)!] != nil {
                cell.rate = rates[(cell.currencyType?.text)!]
                let amount = cell.rate! * self.mainCurrencyValue.value
                cell.amountField.currencyNumber = amount
            }
        }
    }
    
    func synchronizeUpdateToVisibleCells(majorValue:Double){
        for cell in self.tableView.visibleCells {
            let currencyCell:CurrencyCell = cell as! CurrencyCell
            if currencyCell.rate != nil{
                let amount = currencyCell.rate! * majorValue
                if currencyCell.currencyType != currentType{
                   currencyCell.amountField.borderLine.backgroundColor = .lightGray
                   currencyCell.amountField.currencyNumber = amount
                } else {
                   currencyCell.amountField.borderLine.backgroundColor = .blue
                }
            }
        }
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func dataSourceFactory() -> RxTableViewSectionedReloadDataSource<SectionModel<String,Currency>>{
        return RxTableViewSectionedReloadDataSource.init(configureCell: { (dataSource, tableView, indexPath, item) -> UITableViewCell in
            let cell = CurrencyCell(currency: item)
            cell.selectionStyle = .none
            if item.type != self.currentType {
                cell.amountField.currencyNumber = self.mainCurrencyValue.value * self.newestRates.value[(item.type?.text)!]!  //item.rate!
            } else {
                cell.amountField.currencyNumber = self.mainCurrencyValue.value
            }
            cell.amountField.rx.controlEvent([.editingDidBegin])
                .asObservable()
                .subscribe(onNext: {
                    print(cell.amountField.currencyNumber!)
                   self.mainCurrencyValue.accept(cell.amountField.currencyNumber!)
                   self.currentType = item.type!
                   cell.amountField.borderLine.backgroundColor = .blue
                   cell.amountField.currencyText = cell.amountField.text!
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                            self.tableView.moveItemAtIndexPath(indexPath, to: IndexPath(row: 0, section: 0))
                            self.tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
                    }
                }).disposed(by:self.disposeBag)
            
            cell.amountField.rx.controlEvent([.editingChanged])
                .asObservable()
                .subscribe(onNext: {
                    cell.amountField.currencyText = cell.amountField.text!
                    self.mainCurrencyValue.accept(cell.amountField.currencyNumber!) //update to global variable
                    self.synchronizeUpdateToVisibleCells(majorValue:self.mainCurrencyValue.value)
                }).disposed(by: self.disposeBag)
            
            cell.amountField.rx.controlEvent([.editingDidEnd])
                .asObservable()
                .subscribe(onNext: {
                
                cell.amountField.currencyText = cell.amountField.text!
                self.mainCurrencyValue.accept(cell.amountField.currencyNumber!)
                    
                let index = self.items.value.index(where: { $0.type == item.type })
                var items = self.items.value
                items.remove(at: index!)
                items.insert(item, at: 0)
                self.items.accept(items)
               
                }).disposed(by: self.disposeBag)
            return cell
        })
    }

}
