//
//  CurrencyTextField.swift
//  CurrencyConverter
//
//  Created by Leo on 2018/10/10.
//  Copyright © 2018 Leo. All rights reserved.
//

import Foundation
import UIKit

class CurrencyTextField: UITextField {
    
    private let numberFormatter:NumberFormatter = {
        let numberFormatter = NumberFormatter()
            numberFormatter.groupingSeparator = ","
            numberFormatter.groupingSize = 3
            numberFormatter.usesGroupingSeparator = true
            numberFormatter.decimalSeparator = "."
            numberFormatter.numberStyle = .decimal
            numberFormatter.maximumFractionDigits = 0
        return numberFormatter
    }()
    
    lazy var borderLine:UIView = {
       let lineView = UIView()
           lineView.backgroundColor = .lightGray
           lineView.translatesAutoresizingMaskIntoConstraints = false
        return lineView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.borderStyle = UITextField.BorderStyle.none
        self.backgroundColor = UIColor.clear
        self.addSubview(borderLine)
        borderLine.heightAnchor(equalTo: 2)
        borderLine.bottomAnchor(equalTo: self.bottomAnchor, constant: 0)
        borderLine.leadingAnchor(equalTo: self.leadingAnchor, constant: 0)
        borderLine.trailingAnchor(equalTo: self.trailingAnchor, constant: 0)
    }
    
    func updateUnderLineWidth(width:CGFloat){
      // self.removeConstraints(borderLine.constraints)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var currencyText:String{
        set{
            var value = "0"
            if !newValue.isEmpty {
                value = newValue
            }
            value = value.replacingOccurrences(of: ",", with: "")
            self.currencyNumber = Double(value)
            self.text = numberFormatter.string(from: NSNumber(value: self.currencyNumber!))!
            updateUnderLineWidth(width: (self.attributedText?.size().width)!)
        }
        get{
            return numberFormatter.string(from: NSNumber(value: currencyNumber!))!
        }
    }
    
    var currencyNumber: Double?{
        set{
            self.text = numberFormatter.string(from: NSNumber(value: newValue!))!
            updateUnderLineWidth(width: (self.attributedText?.size().width)!)
        }
        get{
            if (self.text?.isEmpty)! {
                return 0
            }
            return Double(exactly: numberFormatter.number(from: self.text!)!)
        }
    }
}




