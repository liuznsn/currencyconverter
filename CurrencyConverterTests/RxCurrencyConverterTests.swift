//
//  RxCurrencyConverterTests.swift
//  CurrencyConverterTests
//
//  Created by Leo on 2018/10/11.
//  Copyright © 2018 Leo. All rights reserved.
//
import Foundation
import RxSwift
import XCTest
import RxTest
import RxDataSources

@testable import CurrencyConverter


class RxCurrencyConverterTests: XCTestCase {

    var scheduler: TestScheduler!
    var subscription: Disposable!
    var converterViewController: ConverterViewController!
    
    override func setUp() {
        super.setUp()
        scheduler = TestScheduler(initialClock: 0)
        
        converterViewController = ConverterViewController()
        
        var currency1 = Currency()
        currency1.rate = 1
        currency1.type = .EUR
        var currency2 = Currency()
        currency2.rate = 129.73
        currency2.type = .JPY
        var currency3 = Currency()
        currency3.rate = 0.89946
        currency3.type = .GBP
    
        
        converterViewController.items.accept([currency1,currency2,currency3])
        converterViewController.newestRates.accept(["EUR":1,"JPY":129.73,"GBP":0.89946])
        converterViewController.viewDidLoad()
    }
    
    override func tearDown() {
        scheduler.scheduleAt(1000) {
            self.subscription.dispose()
        }
        super.tearDown()
    }

    func testCurrencyTextField(){
        let currencyTextField = CurrencyTextField()
        
        currencyTextField.currencyNumber = 100000
        XCTAssertEqual(currencyTextField.text,"100,000")
        XCTAssertEqual(currencyTextField.currencyText,"100,000")
     
        currencyTextField.currencyText = "100,000"
        XCTAssertEqual(currencyTextField.currencyNumber,100000)
        
        currencyTextField.currencyText = ""
        XCTAssertEqual(currencyTextField.currencyNumber,0)


    }
    
    func testTableVisibleCells() {
        
        XCTAssertEqual(converterViewController.tableView.visibleCells.count,3)

        let indexPath = IndexPath(row: 0, section: 0)
        let cell:CurrencyCell = converterViewController.tableView.cellForRow(at: indexPath) as! CurrencyCell
        XCTAssertEqual(cell.currencyType,CurrencyType.EUR)
        
        let indexPath2 = IndexPath(row: 1, section: 0)
        let cell2:CurrencyCell = converterViewController.tableView.cellForRow(at: indexPath2) as! CurrencyCell
        XCTAssertEqual(cell2.currencyType,CurrencyType.JPY)
     
    }
    
    func testSynchronizeUpdateToVisibleCells(){
         XCTAssertEqual(converterViewController.tableView.visibleCells.count,3)
        
        converterViewController.synchronizeUpdateToVisibleCells(majorValue: 100)
        
        let indexPath = IndexPath(row: 0, section: 0)
        let cell:CurrencyCell = converterViewController.tableView.cellForRow(at: indexPath) as! CurrencyCell
        XCTAssertEqual(cell.amountField.text,"100")
        
        let indexPath1 = IndexPath(row: 1, section: 0) //JPY
        let cell1:CurrencyCell = converterViewController.tableView.cellForRow(at: indexPath1) as! CurrencyCell
        
        let indexPath2 = IndexPath(row: 2, section: 0) //GBP
        let cell2:CurrencyCell = converterViewController.tableView.cellForRow(at: indexPath2) as! CurrencyCell

        XCTAssertEqual(cell1.amountField.text,"12,973") // 129.73 * 100
        XCTAssertEqual(cell2.amountField.text,"90") //0.89946 * 100
        
        converterViewController.synchronizeUpdateToVisibleCells(majorValue: 200)

        XCTAssertEqual(cell1.amountField.text,"25,946") // 129.73 * 200

    }
    
    func testUpdateNewestRatesToVisibleCells(){
        
        converterViewController.updateNewestRatesToVisibleCells(rates: ["JPY":128.97,"GBP":0.89422])

        let indexPath1 = IndexPath(row: 1, section: 0) //JPY
        let cell1:CurrencyCell = converterViewController.tableView.cellForRow(at: indexPath1) as! CurrencyCell
        XCTAssertEqual(cell1.amountField.text,"12,897")
        
        let indexPath2 = IndexPath(row: 2, section: 0) //GBP
        let cell2:CurrencyCell = converterViewController.tableView.cellForRow(at: indexPath2) as! CurrencyCell
        XCTAssertEqual(cell2.amountField.text,"89")
        
    }

    
}
